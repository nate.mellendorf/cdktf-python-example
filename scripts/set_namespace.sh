#!/bin/bash
# set -uex

param=$1

# Get the current working dir
cwd=$(pwd)

if [ -z $param ]; then
  # If this is a PR, get the PR#
  echo "Checking for PR"
  # If using GitLab:
  get_env=${CI_MERGE_REQUEST_IID}
  # Else:
  # get_env=$(git -c http.sslVerify=false ls-remote ${CI_REPOSITORY_URL} refs/merge-requests/[0-9]*/head | awk "/$CI_BUILD_REF/"'{print $2}' | cut -d '/' -f3)
elif [ $param == "plan_prod" ]; then
  echo "Param detected: ${param}"
fi

# Get the name of the current repo
project=${CI_PROJECT_NAME}

# If the repo name is not found, error out
if [ -z $project ]; then
    echo "Unable to find project name"
    exit 1
fi

# If we couldn't get the PR#, then run against master
if [ -z $get_env ]; then

    # Create vars
    env="prod"

# If the PR# is found and we're merging to the master branch
elif [ $get_env != "" ] && [ $CI_MERGE_REQUEST_TARGET_BRANCH_NAME == "main" ]; then

    # Create vars
    env="pr-${get_env}"

fi

# Make sure the environment is defined
if [ -z $env ]; then
    # If not found, error out
    echo "TF Environment is not defined."
    exit 1
fi

# Create Terraform variable used for namespacing resources
export ENV="$env"
echo "+--------------------------------+"
echo "|          ENV: ${env}           |"
echo "+--------------------------------+"
